# Resurs Bank - Magento 2 module - Simplified Flow

## Description

Simplified Flow API implementation for Magento 2 module.

---

## Prerequisites

* [Magento 2](https://devdocs.magento.com/guides/v2.4/install-gde/bk-install-guide.html) [Supports Magento 2.4.1+]

---

#### 1.2.1

* Updated composer metadata.

#### 1.5.2

* Replaced logotypes.
* Added PHP 8.1 support.
